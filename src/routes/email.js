const { Router } = require('express');
const router = Router();
const email_controller = require('../controllers/email.controller');

router.post('/', email_controller.upload.single('file'), email_controller.sendEmail);

module.exports = router;
