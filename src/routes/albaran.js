const { Router } = require('express');
const router = Router();
const albaran_controller = require('../controllers/albaran.controller');

router.post('/', albaran_controller.crearAlbaran);
router.get('/', albaran_controller.getAlbaranes);
router.delete('/:albaranId', albaran_controller.deleteAlbaran);

module.exports = router;
