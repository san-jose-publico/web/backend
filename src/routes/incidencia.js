const { Router } = require('express');
const router = Router();
const incidencia_controller = require('../controllers/incidencia.controller');

router.post('/', incidencia_controller.crearIncidencia);
router.get('/', incidencia_controller.getIncidencias);
router.get('/all', incidencia_controller.getIncidenciasAll);
router.get('/:incidenciaId', incidencia_controller.getIncidencia);
router.get('/proyecto/:proyectoId', incidencia_controller.getIncidenciasProyecto);
router.get('/inspecciones/:incidenciaId', incidencia_controller.getInspeccionFromIncidencia);
router.patch('/:incidenciaId/blockChainId/:blockChainId', incidencia_controller.updateBlockChainId);
router.patch('/:incidenciaId/status', incidencia_controller.updateStatus);
router.delete('/:incidenciaId', incidencia_controller.deleteIncidencia);

module.exports = router;
