const { Router } = require('express');
const router = Router();
const papelera_controller = require('../controllers/papelera.controller');

router.get('/', papelera_controller.getObjetosEliminados);
router.post('/', papelera_controller.createObjetoEliminado);
router.patch('/:objetoEliminadoId', papelera_controller.updateStatusObjeto);
router.delete('/:objetoEliminadoId', papelera_controller.deleteObjeto);

module.exports = router;
