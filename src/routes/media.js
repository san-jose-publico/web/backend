const { Router } = require('express');
const router = Router();
const media_controller = require('../controllers/media.controller');

router.post('/', media_controller.handleUpload);
router.get('/download/:filename', media_controller.downloadFile);
router.get('/list', media_controller.getListMedia);
router.delete('/:filename', media_controller.deleteFile);

// router.get('/', media_controller.getInspecciones);

module.exports = router;
