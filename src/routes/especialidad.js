const { Router } = require('express');
const router = Router();
const especialidad_controller = require('../controllers/especialidad.controller');

router.post('/', especialidad_controller.crearEspecialidad);
router.get('/', especialidad_controller.getEspecialidades);
router.delete('/:especialidadId', especialidad_controller.eliminarEspecialidad);

module.exports = router;
