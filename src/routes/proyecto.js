const { Router } = require('express');
const router = Router();
const proyecto_controller = require('../controllers/proyecto.controller');

router.post('/', proyecto_controller.crearProyecto);
router.get('/all', proyecto_controller.getProyectosAll);
router.get('/', proyecto_controller.getProyectos);
router.get('/:proyectoId', proyecto_controller.getProyecto);
router.patch('/:proyectoId', proyecto_controller.editarDocumentacionProyecto);
router.put('/:proyectoId/', proyecto_controller.actualizarProyecto);

module.exports = router;
