const { Router } = require('express');
const router = Router();
const inspeccion_controller = require('../controllers/inspeccion.controller');

router.post('/', inspeccion_controller.crearInspeccion);
router.get('/', inspeccion_controller.getInspecciones);
router.get('/proyecto/:proyectoId', inspeccion_controller.getInspeccionesProyecto);
router.delete('/:inspeccionId', inspeccion_controller.deleteInspeccion);

module.exports = router;
