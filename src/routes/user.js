const { Router } = require('express');
const router = Router();
const user_controller = require('../controllers/user.controller');

router.post('/login', user_controller.loginUsuario);
router.get('/especialidad/:especialidad', user_controller.getUsuariosEspecialidad);
router.get('/:usuarioId', user_controller.getUsuario);
router.get('/', user_controller.getUsuarios);
router.put('/:usuarioId', user_controller.actualizarUsuario);
router.post('/', user_controller.registrarUsuario);
router.patch('/status/:usuarioId', user_controller.statusUsuario);
router.patch('/:usuarioId/ultimoAcceso', user_controller.actualizarUltimoAcceso);

module.exports = router;
