
const Proyecto = require('../models/Proyecto');
const User = require('../models/User');
const { verifyToken } = require('../helpers/verifyToken');
const moment = require('moment');

const crearProyecto =  async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }
    
    try{
        const {...object} = req.body;
        const proyecto = new Proyecto(object);
    
        const numProyectos = await Proyecto.find().countDocuments() + 1;
        proyecto.codigo  = numProyectos.toString().padStart(6,'0');
        proyecto.fechaCreacion = new Date();
        proyecto.activo = true;

        await proyecto.save();

        res.json({
            ok: true,
            msg: 'Proyecto creado',
            data: proyecto
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error creando proyecto'+err
        });
    }
   
};

const getProyectos = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        const desde = Number(req.query.desde) || 0;
        const regPorPag = Number(process.env.DOCSPERPAGE);
        let proyectos;
        let total = 0;
        let palabras = [];

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaCreacion': {
                $gte: fechaIni,
                $lte: fechaFin
            }
        };

        if(filtro && palabra) {
            if(filtro !== 'autor'){
                query[filtro] = {
                    $regex: new RegExp(palabra, 'i')
                };
            }
            else{
                palabras = palabra.split(',');
            }
        }

        if(palabras.length > 0){
            let projects = [];
            for(let palabra of palabras){
                query[filtro] = palabra;
                proyectos = await Proyecto.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});

                if(proyectos.length > 0){
                    projects.push(...proyectos);
                }
            }
            proyectos = projects;

        }
        else{
            if((!filtro && !palabra) || (filtro && palabra)){
                proyectos = await Proyecto.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
            }

            if(tipo === 'autor'){
                proyectos = await Promise.all(proyectos.map(async proyecto => {
                    const autor = await User.findById(proyecto.autor);
                    const nombreAutor = autor.nombre;
                    const apellidos =  autor.apellidos;
                    return { ...proyecto.toObject(), nombreAutor, apellidos };
                }));

                proyectos.sort((a, b) => {
                    const nombreA = a.nombreAutor + ' ' + a.apellidos;
                    const nombreB = b.nombreAutor + ' ' + b.apellidos;
                    return num * nombreA.localeCompare(nombreB, 'es', { sensitivity: 'base' });
                });

            }
        }

        if(proyectos){
            if(palabras.length > 0){
                total = proyectos.length;
            }
            else{
                total = await Proyecto.countDocuments(query);
            }
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener proyectos',
            data: proyectos,
            recordsTotal: total
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo proyectos',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getProyectosAll = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const proyectoId = req.params.proyectoId;

        const proyecto = await Proyecto.find();

        res.status(200).json({
            ok: true,
            msg: 'obtener proyectos',
            data: proyecto,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo proyectos',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getProyecto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const proyectoId = req.params.proyectoId;

        const proyecto = await Proyecto.findById(proyectoId);

        res.status(200).json({
            ok: true,
            msg: 'obtener proyecto',
            data: proyecto,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo proyecto',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const editarDocumentacionProyecto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const proyectoId = req.params.proyectoId;
        const documentos = req.body.documentos;

        const proyecto = await Proyecto.findByIdAndUpdate(
            proyectoId,
            { documentos: documentos},
            { new: true }
        );

        res.status(200).json({
            ok: true,
            msg: 'obtener proyecto',
            data: proyecto,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo proyecto',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const actualizarProyecto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const proyectoId = req.params.proyectoId;
        const {...object} = req.body;  

        const proyecto = await Proyecto.findByIdAndUpdate(proyectoId, object, { new: true });

        res.status(200).json({
            ok: true,
            msg: 'obtener proyecto',
            data: proyecto,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo proyecto',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

module.exports = { crearProyecto, getProyectos, getProyectosAll, getProyecto, editarDocumentacionProyecto, actualizarProyecto } 