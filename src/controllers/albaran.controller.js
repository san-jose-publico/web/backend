
const Albaran = require('../models/Albaran');
const { verifyToken } = require('../helpers/verifyToken');

const crearAlbaran =  async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }

    try{
        const { ...object } = req.body;
        const numero = object.numero;
        const existente = await Albaran.findOne({ numero });

        if (existente) {
            return res.status(400).json({
                ok: false,
                msg: 'Ya existe un albaran con este numero'
            });
        }
        const albaran = new Albaran(object);
        
        await albaran.save();

        res.status(200).
        json({
            ok: true,
            msg: 'albaran creada',
            data: albaran
        })
    }
    catch(err){
        return  res.status(500).json({
            ok: false,
            msg: 'Error creando albaran'+err
        });
    }
   
};

const getAlbaranes = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        const desde = Number(req.query.desde) || 0;
        const regPorPag = Number(process.env.DOCSPERPAGE);
        let albaranes;
        let total = 0;

        // if(fechaIni && fechaFin){
        //     fechaIni = new Date(req.query.fechaIni);
        //     fechaFin = new Date(req.query.fechaFin);
        //     fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        // }
        // else{
        //     fechaIni = new Date("1900-01-01");
        //     fechaFin = new Date("9999-12-31");
        // }

        const query = {
            'activo': true
        }

        if(filtro && palabra) {
            query[filtro] = {
                $regex: new RegExp(palabra, 'i')
            };
        }
        
        albaranes = await Albaran.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
        if(albaranes){
            total = await Albaran.countDocuments(query);
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener albaranes',
            data: albaranes,
            recordsTotal: total
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo albaranes',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const deleteAlbaran =  async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }

    try{
        const uid = req.params.albaranId;        
        await Albaran.findOneAndDelete({_id: uid});
        
        res.status(200).json({
            ok: true,
            msg: 'albaran eliminado',
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error eliminando albaran'+ err
        });
    }
   
};

module.exports = { crearAlbaran, getAlbaranes, deleteAlbaran  } 