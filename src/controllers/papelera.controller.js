
const Incidencia = require('../models/Incidencia');
const Proyecto = require('../models/Proyecto');
const Inspeccion = require('../models/Inspeccion');
const Albaran = require('../models/Albaran');
const User = require('../models/User');
const { verifyToken } = require('../helpers/verifyToken');
const moment = require('moment');
const Papelera = require('../models/Papelera');


const createObjetoEliminado =  async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try{
        const {...object} = req.body;
        const tipo = object.tipo;
        const objetoId = object.objetoId;

        const modelos = {
            'incidencia': Incidencia,
            'inspeccion': Inspeccion,
            'albaran': Albaran,
            'usuario': User
        };

        const modelo = modelos[tipo];

        await modelo.findOneAndUpdate(
            { _id: objetoId },
            { activo: false },
            { new: true }
        );

        const objectoEliminado = new Papelera(object);
        await objectoEliminado.save();

        res.json({
            ok: true,
            msg: 'objetoEliminado creado',
            data: objectoEliminado
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error creando objetoEliminado'+ err
        });
    }
   
};

const getObjetosEliminados = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol !== 'admin'){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;

        const num = orden === 'desc' ? -1 : 1;
        const desde = Number(req.query.desde) || 0;
        const regPorPag = Number(process.env.DOCSPERPAGE);
        let objetosEliminados;
        let total = 0;

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaEliminacion': {
                $gte: fechaIni,
                $lte: fechaFin
            }
        };

        if(filtro && palabra) {
            query[filtro] = {
                $regex: new RegExp(palabra, 'i')
            };
        }

        objetosEliminados = await Papelera.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
        total = await Papelera.countDocuments(query);

        res.status(200).json({
            ok: true,
            msg: 'obtener objetos eliminados',
            data: objetosEliminados,
            recordsTotal: total
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo objetos eliminados',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const updateStatusObjeto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol !== 'admin'){
        return res.status(401).send("Sin autorización");
    }

    try {
        const { objetoId, tipo } = req.body;
        const objetoEliminadoId = req.params.objetoEliminadoId;

        const modelos = {
            'incidencia': Incidencia,
            'inspeccion': Inspeccion,
            'albaran': Albaran,
            'usuario': User
        };

        const modelo = modelos[tipo];

        encontrado = await modelo.findOneAndUpdate(
            { _id: objetoId },
            { activo: true },
            { new: true }
        );

        await Papelera.findOneAndDelete({_id: objetoEliminadoId});

        res.status(200).json({
            ok: true,
            msg: 'objeto restaurado',
        });
    } catch (error) {
        res.status(500).json({
            status: 'objeto restaurado' + error,
            message: error
        });
    }    
}

const deleteObjeto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol === 'usuario'){
        return res.status(401).send("Sin autorización");
    }

    try {
        const objetoEliminadoId = req.params.objetoEliminadoId;

        Papelera.findOneAndDelete({_id: objetoEliminadoId});

        res.status(200).json({
            ok: true,
            msg: 'objeto eliminado',
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error eliminando objeto',
            message: error
        });
    }
}





module.exports = { createObjetoEliminado, getObjetosEliminados, updateStatusObjeto, deleteObjeto } 