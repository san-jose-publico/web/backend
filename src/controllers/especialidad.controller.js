
const Especialidad = require('../models/Especialidad');
const { verifyToken } = require('../helpers/verifyToken');

const crearEspecialidad =  async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }
    
    try{
        const { ...object } = req.body;
        const nombre = object.nombre;
        const existente = await Especialidad.findOne({ nombre });

        if (existente) {
            return res.status(400).json({
                ok: false,
                msg: 'Ya existe una especialidad con este nombre'
            });
        }
        const especialidad = new Especialidad(object);
        
        await especialidad.save();

        res.status(200).
        json({
            ok: true,
            msg: 'especialidad creada',
            data: especialidad
        })
    }
    catch(err){
        return  res.status(500).json({
            ok: false,
            msg: 'Error creando especialidad'+err
        });
    }
   
};

const getEspecialidades =  async (req, res) => {
    try{
        let especialidadades = await Especialidad.find();
        
        res.status(200).
        json({
            ok: true,
            msg: 'especialidades obtenidas',
            data: especialidadades
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error obteniendo especialidades'+err
        });
    }
   
};

const eliminarEspecialidad =  async (req, res) => {
    try{
        const uid = req.params.especialidadId;        
        await Especialidad.findOneAndDelete({_id: uid});
        
        res.status(200).json({
            ok: true,
            msg: 'especialidad eliminada',
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error eliminando especialidad'+err
        });
    }
   
};

module.exports = { crearEspecialidad, getEspecialidades, eliminarEspecialidad  } 