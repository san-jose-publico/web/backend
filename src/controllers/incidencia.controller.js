
const Incidencia = require('../models/Incidencia');
const Proyecto = require('../models/Proyecto');
const Inspeccion = require('../models/Inspeccion');
const User = require('../models/User');
const { verifyToken } = require('../helpers/verifyToken');
const moment = require('moment');

const crearIncidencia =  async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }
    
    try{
        const {...object} = req.body;
        const incidencia = new Incidencia(object);
    
        incidencia.fechaCreacion = new Date();
        incidencia.activo = true;
        const numIncidencias = await Incidencia.find().countDocuments() + 1;

        incidencia.codigo =  'INC' + numIncidencias.toString().padStart(3,'0');

        await Proyecto.findOneAndUpdate({
            _id: incidencia.proyecto
        }, {
            $inc: {
                'numInc': 1
            }
        });

        await incidencia.save();

        res.json({
            ok: true,
            msg: 'incidencia creada',
            data: incidencia
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error creando incidencia'+err
        });
    }
   
};

const getIncidencias = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        const desde = Number(req.query.desde) || 0;
        const regPorPag = Number(process.env.DOCSPERPAGE);
        let incidencias;
        let total = 0;
        let palabras = [];

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaCreacion': {
                $gte: fechaIni,
                $lte: fechaFin
            },
            'activo': true
        };

        if(filtro && palabra) {
            if(filtro !== 'proyecto' && filtro !== 'autor'){
                console.log('entro aqui', filtro, palabra);
                query[filtro] = {
                    $regex: new RegExp(palabra, 'i')
                };
            }
            else{
                palabras = palabra.split(',');
            }
        }

        if(palabras.length > 0){
            let incs = [];
            for(let palabra of palabras){
                query[filtro] = palabra;
                incidencias = await Incidencia.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});

                if(incidencias.length > 0){
                    incs.push(...incidencias);
                }
            }
            incidencias = incs;
        }
        else{
            if((!filtro && !palabra) || (filtro && palabra)){
                incidencias = await Incidencia.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
            }
            
            if(tipo === 'autor'){
                incidencias = await Promise.all(incidencias.map(async proyecto => {
                    const autor = await User.findById(proyecto.autor);
                    const nombre = autor.nombre;
                    const apellidos =  autor.apellidos;
                    return { ...proyecto.toObject(), nombre, apellidos };
                }));

                incidencias.sort((a, b) => {
                    const nombreA = a.nombre + ' ' + a.apellidos;
                    const nombreB = b.nombre + ' ' + b.apellidos;
                    return num * nombreA.localeCompare(nombreB, 'es', { sensitivity: 'base' });
                });

            }
        }

        if(incidencias){
            if(palabras.length > 0){
                total = incidencias.length;
            }
            else{
                total = await Incidencia.countDocuments(query);
            }
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener incidencias',
            data: incidencias,
            recordsTotal: total
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo incidencias',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getIncidenciasAll = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const incidencias = await Incidencia.find();

        res.status(200).json({
            ok: true,
            msg: 'obtener incidencias',
            data: incidencias,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo incidencias',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getIncidencia = async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const incidenciaId = req.params.incidenciaId;

        const incidencia = await Incidencia.findById(incidenciaId);

        res.status(200).json({
            ok: true,
            msg: 'obtener incidencia',
            data: incidencia,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo incidencia',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getIncidenciasProyecto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        let incidencias;
        let palabras = [];
        const proyectoId = req.params.proyectoId;

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaCreacion': {
                $gte: fechaIni,
                $lte: fechaFin
            },
            'activo': true
        };

        query['proyecto'] = proyectoId;

        if(filtro && palabra) {
            if(filtro !== 'autor'){
                query[filtro] = {
                    $regex: new RegExp(palabra, 'i')
                };
            }
            else{
                palabras = palabra.split(',');
            }
        }

        if(palabras.length > 0){
            let incs = [];
            for(let palabra of palabras){
                query[filtro] = palabra;
                incidencias = await Incidencia.find(query).sort({[tipo]: num}).collation({locale: "es", strength: 2});
                if(incidencias.length > 0){
                    incs.push(...incidencias);
                }
            }
            incidencias = incs;
        }
        else{
            if(!filtro && !palabra || filtro && palabra){
                incidencias = await Incidencia.find(query).sort({[tipo]: num}).collation({locale: "es", strength: 2});
            }

            if(tipo === 'autor'){
                incidencias = await Promise.all(incidencias.map(async proyecto => {
                    const autor = await User.findById(proyecto.autor);
                    const nombre = autor.nombre;
                    const apellidos =  autor.apellidos;
                    return { ...proyecto.toObject(), nombre, apellidos };
                }));

                incidencias.sort((a, b) => {
                    const nombreA = a.nombre + ' ' + a.apellidos;
                    const nombreB = b.nombre + ' ' + b.apellidos;
                    return num * nombreA.localeCompare(nombreB, 'es', { sensitivity: 'base' });
                });

            }
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener incidencias',
            data: incidencias,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo incidencias',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getInspeccionFromIncidencia = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        let inspecciones;
        let palabras = [];
        const incidenciaId = req.params.incidenciaId;

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaCreacion': {
                $gte: fechaIni,
                $lte: fechaFin
            }
        };

        query['incidencia'] = incidenciaId;

        if(filtro && palabra) {
            if(filtro !== 'autor'){
                query[filtro] = {
                    $regex: new RegExp(palabra, 'i')
                };
            }
            else{
                palabras = palabra.split(',');
            }
        }

        if(palabras.length > 0){
            let incs = [];
            for(let palabra of palabras){
                query[filtro] = palabra;
                inspecciones = await Inspeccion.find(query).sort({[tipo]: num}).collation({locale: "es", strength: 2});
                if(inspecciones.length > 0){
                    incs.push(...inspecciones);
                }
            }
            inspecciones = incs;
        }
        else{
            inspecciones = await Inspeccion.find(query).sort({[tipo]: num}).collation({locale: "es", strength: 2});
            
            if(tipo === 'autor'){
                inspecciones = await Promise.all(inspecciones.map(async proyecto => {
                    const autor = await User.findById(proyecto.autor);
                    const nombre = autor.nombre;
                    const apellidos =  autor.apellidos;
                    return { ...proyecto.toObject(), nombre, apellidos };
                }));

                inspecciones.sort((a, b) => {
                    const nombreA = a.nombre + ' ' + a.apellidos;
                    const nombreB = b.nombre + ' ' + b.apellidos;
                    return num * nombreA.localeCompare(nombreB, 'es', { sensitivity: 'base' });
                });
            }
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener inspecciones de una incidencia',
            data: inspecciones,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo inspecciones de una incidencia',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const updateStatus = async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const incidenciaId = req.params.incidenciaId;
        const estado = req.body.estado;
        const today = new Date();
        const incidencia = await Incidencia.findByIdAndUpdate(
            incidenciaId,
            { estado: estado, fechaEstadoCerrada: today},
            { new: true }
        );

        res.status(200).json({
            ok: true,
            msg: 'actualizar estado',
            data: incidencia,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error actualizando incidencia',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const updateBlockChainId = async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const incidenciaId = req.params.incidenciaId;
        const blockChainId = req.body.blockChainId;

        //Necesita el $set porque el campo no existe previamente
        const incidencia = await Incidencia.findByIdAndUpdate(
            incidenciaId,
            { $set: { blockChainId } },
            { new: true }
        );

        res.status(200).json({
            ok: true,
            msg: 'actualizar blockChainId',
            data: incidencia,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error actualizando blockChainId',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const deleteIncidencia = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol === 'usuario'){
        return res.status(401).send("Sin autorización");
    }

    try {
        const uid = req.params.incidenciaId;

        let incidencia = await Incidencia.findOneAndDelete({_id: uid});
        numInsDeleted = await Inspeccion.countDocuments({ incidencia: uid });

        await Inspeccion.deleteMany(
            {incidencia: uid}              
        );

        //La actualización del contador se hace por separado para manejar las condiciones (incidencia o inspeccion > 0) por separado
        await Proyecto.findOneAndUpdate({
            _id: incidencia.proyecto,
            numInc: { $gt: 0 }
        }, {
            $inc: {
                'numInc': -1,
            }
        });

        await Proyecto.findOneAndUpdate({
            _id: incidencia.proyecto,
            numIns: { $gt: 0 }
        }, {
            $inc: {
                'numIns': -numInsDeleted,
            }
        });

        res.status(200).json({
            ok: true,
            msg: 'eliminar incidencia',
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error eliminando incidencia',
            message: error
        });
    }
}

module.exports = { crearIncidencia, getIncidencias, getIncidenciasAll, getIncidencia, getIncidenciasProyecto, getInspeccionFromIncidencia, updateStatus, deleteIncidencia, updateBlockChainId } 