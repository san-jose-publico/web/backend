const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
require("aws-sdk/lib/maintenance_mode_message").suppress = true;

const AWS_ACCESS_KEY_ID = 'AKIA55CI3RZ6URIWIJET'
const AWS_SECRET_ACCESS_KEY = ''
const AWS_BUCKET_NAME = 'mediafiles-csj'
const AWS_REGION = 'eu-west-3'

// const s3 = new AWS.S3({
//   accessKeyId: AWS_ACCESS_KEY_ID,
//   secretAccessKey: AWS_SECRET_ACCESS_KEY
// });

AWS.config.update({
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
    region: AWS_REGION
})

const s3 = new AWS.S3();

// const s3 = new AWS.S3({
//     accessKeyId: AWS_ACCESS_KEY_ID,
//     secretAccessKey: AWS_SECRET_ACCESS_KEY,
//     region: AWS_REGION
// });

const upload = multer({
    storage: multerS3({
        bucket: AWS_BUCKET_NAME,
        s3: s3,
        ACL: 'public-read',
        key: (req, file, cb) => {
            cb(null, file.originalname);
        }
    })
});

const handleUpload = (req, res) => {
    upload.array('files')(req, res, (err) => {
        if (err) {
            return res.status(400).json({ error: err.message });
        }
        const locations = req.files.map(file => file.location);

        res.send('Successfully uploaded ' + locations.join(', ') + ' location!');
    });
};

const getListMedia = async (req, res) => {
    let result = await s3.listObjectsV2({
        Bucket: AWS_BUCKET_NAME
    }).promise()


    let r = result.Contents.map(item => item.Key);
    res.send(r);
};

const downloadFile = async (req, res) => {
    const filename = req.params.filename
    // let r = await s3.getObject({
    //     Bucket: AWS_BUCKET_NAME,
    //     Key: filename
    // }).promise()

    let r = await s3.getSignedUrl("getObject", {
        Bucket: AWS_BUCKET_NAME,
        Key: filename
    })

    res.send(r);

}

const deleteFile = async (req, res) => {
    const filename = req.params.filename
    let r = await s3.deleteObject({
        Bucket: AWS_BUCKET_NAME,
        Key: filename
    }).promise()

    res.send("File deleted Successfully");
}

module.exports = {
    handleUpload, getListMedia, downloadFile, deleteFile
};

// s3.putObject(params, function (perr, pres) {
//   if (perr) {
//       console.log("Error uploading data: ", perr);
//   } else {
//       console.log("Successfully uploaded data to myBucket/myKey");
//   }
// });


// s3.getObject(params, function(err, data){
//   if(err) console.log(err, err.stack);
//   else console.log(data);

// });
