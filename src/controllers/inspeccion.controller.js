
const Inspeccion = require('../models/Inspeccion');
const Proyecto = require('../models/Proyecto');
const User = require('../models/User');
const { verifyToken } = require('../helpers/verifyToken');
const moment = require('moment');

const crearInspeccion =  async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }
    
    try{
        const {...object} = req.body;
        const inspeccion = new Inspeccion(object);
    
        inspeccion.fechaCreacion = new Date();
        inspeccion.activo = true;

        await Proyecto.findOneAndUpdate({
            _id: inspeccion.proyecto
        }, {
            $inc: {
                'numIns': 1
            }
        });

        await inspeccion.save();

        res.json({
            ok: true,
            msg: 'inspeccion creada',
            data: inspeccion
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error creando inspeccion'+err
        });
    }
   
};

const getInspecciones = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        const desde = Number(req.query.desde) || 0;
        const regPorPag = Number(process.env.DOCSPERPAGE);
        let inspecciones;
        let total = 0;
        let palabras = [];

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaCreacion': {
                $gte: fechaIni,
                $lte: fechaFin
            },
            'activo': true
        };

        if(filtro && palabra) {
            if(filtro !== 'autor' && filtro !== 'incidencia'){
                query[filtro] = {
                    $regex: new RegExp(palabra, 'i')
                };
            }
            else{
                if(filtro === 'incidencia'){
                    query[filtro] = palabra;
                }
                else{
                    palabras = palabra.split(',');
                }
            }
        }

        if(palabras.length > 0){
            let ins = [];
            for(let palabra of palabras){
                query[filtro] = palabra;
                inspecciones = await Inspeccion.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
                if(inspecciones.length > 0){
                    ins.push(...inspecciones);
                }
            }
            inspecciones = ins;
        }
        else{
            if((!filtro && !palabra) || (filtro && palabra)){
                inspecciones = await Inspeccion.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
            }
        
            if(tipo === 'autor'){
                inspecciones = await Promise.all(inspecciones.map(async proyecto => {
                    const autor = await User.findById(proyecto.autor);
                    const nombre = autor.nombre;
                    const apellidos =  autor.apellidos;
                    return { ...proyecto.toObject(), nombre, apellidos };
                }));

                inspecciones.sort((a, b) => {
                    const nombreA = a.nombre + ' ' + a.apellidos;
                    const nombreB = b.nombre + ' ' + b.apellidos;
                    return num * nombreA.localeCompare(nombreB, 'es', { sensitivity: 'base' });
                });
            }
        }

        if(inspecciones){
            if(palabras.length > 0){
                total = inspecciones.length;
            }
            else{
                total = await Inspeccion.countDocuments(query);
            }
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener inspecciones',
            data: inspecciones,
            recordsTotal: total
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo inspecciones',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getInspeccionesProyecto = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        let inspecciones;
        let palabras = [];

        const proyectoId = req.params.proyectoId;

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            'fechaCreacion': {
                $gte: fechaIni,
                $lte: fechaFin
            }
        };

        query['proyecto'] = proyectoId;

        if(filtro && palabra) {
            if(filtro !== 'autor'){
                query[filtro] = {
                    $regex: new RegExp(palabra, 'i')
                };
            }
            else{
                if(filtro === 'incidencia'){
                    query[filtro] = palabra;
                }
                else{
                    palabras = palabra.split(',');
                }
            }
        }

        if(palabras.length > 0){
            let ins = [];
            for(let palabra of palabras){
                query[filtro] = palabra;
                inspecciones = await Inspeccion.find(query).sort({[tipo]: num}).collation({locale: "es", strength: 2});
                if(inspecciones.length > 0){
                    ins.push(...inspecciones);
                }
            }
            inspecciones = ins;
        }
        else{
            if((!filtro && !palabra) || (filtro && palabra)){
                inspecciones = await Inspeccion.find(query).sort({[tipo]: num}).collation({locale: "es", strength: 2});
            }

            if(tipo === 'autor'){
                inspecciones = await Promise.all(inspecciones.map(async proyecto => {
                    const autor = await User.findById(proyecto.autor);
                    const nombre = autor.nombre;
                    const apellidos =  autor.apellidos;
                    return { ...proyecto.toObject(), nombre, apellidos };
                }));

                inspecciones.sort((a, b) => {
                    const nombreA = a.nombre + ' ' + a.apellidos;
                    const nombreB = b.nombre + ' ' + b.apellidos;
                    return num * nombreA.localeCompare(nombreB, 'es', { sensitivity: 'base' });
                });
            }
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener inspecciones',
            data: inspecciones,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo inspecciones',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const deleteInspeccion = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol === 'usuario'){
        return res.status(401).send("Sin autorización");
    }

    try {
        const uid = req.params.inspeccionId;

        let inspeccion = await Inspeccion.findOneAndDelete({_id: uid});

        await Proyecto.findOneAndUpdate({
            _id: inspeccion.proyecto,
            numIns: { $gt: 0 }
        }, {
            $inc: {
                'numIns': -1,
            }
        });

        res.status(200).json({
            ok: true,
            msg: 'eliminar inspeccion',
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error eliminando inspeccion',
            message: error
        });
    }
}

module.exports = { crearInspeccion, getInspecciones, getInspeccionesProyecto, deleteInspeccion } 