const nodemailer = require("nodemailer");
const { verifyToken } = require('../helpers/verifyToken');
const multer = require('multer');

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const sendEmail = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol !== 'admin'){
        return res.status(401).send("Sin autorización");
    }

    try {
        const gmail = 'bexreal.team@gmail.com';
        const { originalname, buffer }  = req.file;
        const { destinatarios, asunto, cuerpo } = req.body;
        
        const transporter = nodemailer.createTransport({
            service: "Gmail",
            port: 465,
            secure: true,
            auth: {
                user: gmail,
                pass: "ngjs mhrn mwxt lyoz",
            },
        });

        var mailOptions = {
            from: gmail,
            to: destinatarios,
            subject: asunto,
            text: cuerpo,
            attachments: [
                {
                  filename: originalname,
                  content: buffer,
                  encoding: 'base64'
                }
              ]
            // html: "<b>Hello world?</b>",
          };
          
        transporter.sendMail(mailOptions);

        res.status(200).json({
            ok: true,
            msg: 'enviar email',
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error enviando email',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

module.exports = { sendEmail, upload } 