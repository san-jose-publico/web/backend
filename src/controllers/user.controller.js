
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { verifyToken } = require('../helpers/verifyToken');
const moment = require('moment');


const registrarUsuario =  async (req, res) => {
    try{
        const {username, password, fechaAlta, activo, ...object } = req.body;
        const salt = bcrypt.genSaltSync();
        const cpassword = bcrypt.hashSync(password, salt);
        const user = new User(object);

        user.password = cpassword;
        user.fechaAlta = new Date();
        user.activo = true;
        user.username = username;

        let existeUsername = await User.findOne ({ username }).select("-password");

        if(existeUsername !== null){
            return  res.status(400).json({
                ok: false,
                msg: 'Username ya existe'
            });
        }

        await user.save();
        const token = jwt.sign({ _id: user._id }, process.env.TOKENSECRET);

        return res.json({
            ok: true,
            msg: 'Usuario registrado',
            usuario: user,
            token: token
        })
    }
    catch(err){
        return  res.status(400).json({
            ok: false,
            msg: 'Error creando usuario' + err
        });
    }
   
};

const getUsuarios = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let { orden, tipo, filtro, palabra, fechaIni, fechaFin } = req.query;
        const num = orden === 'desc' ? -1 : 1;
        const desde = Number(req.query.desde) || 0;
        const regPorPag = Number(process.env.DOCSPERPAGE);
        let usuarios;
        let total = 0;

        if(fechaIni && fechaFin){
            fechaIni = new Date(req.query.fechaIni);
            fechaFin = new Date(req.query.fechaFin);
            fechaFin = moment(req.query.fechaFin).endOf('day').toDate();
        }
        else{
            fechaIni = new Date("1900-01-01");
            fechaFin = new Date("9999-12-31");
        }

        const query = {
            $or: [
                // Para que capte los usuarios que tampoco tienen ultimoAcceso porque nunca han logueado
                { 'ultimoAcceso': { $exists: false } },
                {
                    'ultimoAcceso': {
                        $gte: fechaIni,
                        $lte: fechaFin
                    }
                }
            ]
        };

        if(filtro && palabra) {
            query[filtro] = {
                $regex: new RegExp(palabra, 'i')
            };
        }
                
        usuarios = await User.find(query).skip(desde).limit(regPorPag).sort({[tipo]: num}).collation({locale: "es", strength: 2});
        if(usuarios){
            total = await User.countDocuments(query);
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener usuarios',
            data: usuarios,
            recordsTotal: total
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo usuarios',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getUsuariosEspecialidad = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol'))){
        return res.status(401).send("Sin autorización");
    }

    try {
        let especialidad = req.params.especialidad;
        const usuarios = await User.find({
            especialidad: especialidad,
        }).select('email');

        res.status(200).json({
            ok: true,
            msg: 'obtener usuarios por especialidad',
            data: usuarios,
        });
        
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo usuarios',
            message: error
        });

        console.error('Validation Error:', error.message);
        throw error;
    }
}

const getUsuario = async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }
    try {
        let uid = req.params.usuarioId;
        let usuarioBD = await User.findById(uid);

        if (usuarioBD === null) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no encontrado',
                data: null
            });
        }

        res.status(200).json({
            ok: true,
            msg: 'obtener usuario',
            data: usuarioBD
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo usuario',
            message: error
        });
    }
}

const actualizarUsuario = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || ((!req.headers.hasOwnProperty('uid')) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol !== 'admin')){
        return res.status(401).send("Sin autorización");
    }

    try {
        const uid = req.params.usuarioId;
        const {...object} = req.body;

        if(!object.password.startsWith('$')){
            const salt = bcrypt.genSaltSync();
            const cpassword = bcrypt.hashSync(object.password, salt);
            object.password = cpassword;
        }

        let usuarioBD = await User.findByIdAndUpdate(uid, object, { new: true });
        res.status(200).json({
            ok: true,
            msg: 'Actualización con éxito',
            data: usuarioBD
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error actualizando usuario'+ error,
            message: error
        });
    }
}

const actualizarUltimoAcceso = async (req, res) => {
    if(!(verifyToken(req.headers.token))){
        return res.status(401).send("Sin autorización");
    }

    try {
        const uid = req.params.usuarioId;
        const ultimoAcceso = req.body.ultimoAcceso;
        
        // console.log(uid, ultimoAcceso);
        // const comunidades = new Map();
        // comunidades.set('AN', 'Andalucía');
        // comunidades.set('AR', 'Aragón' );
        // comunidades.set('AS', 'Asturias');
        // comunidades.set('CN', 'Canarias');
        // comunidades.set('CB', 'Cantabria');
        // comunidades.set('CM', 'Castilla-La Mancha');
        // comunidades.set('CL', 'Castilla y León');
        // comunidades.set('CT', 'Cataluña​');
        // comunidades.set('EX', 'Extremadura');
        // comunidades.set('GA', 'Galicia​');
        // comunidades.set('IB', 'las Islas Baleares');
        // comunidades.set('RI', 'La Rioja');
        // comunidades.set('MD', 'Madrid');
        // comunidades.set('MC', 'Murcia');
        // comunidades.set('NC', 'Navarra​');
        // comunidades.set('PV', 'País Vasco');
        // comunidades.set('VC', 'Comunidad Valenciana');
        // const paises = require("../data/paises.json");

        // //req.socket?.remoteAddress o req.connection.remoteAddres
        // const ip = req.headers['x-forwarded-for']?.split(',').shift() || req.connection.remoteAddress;
        // // var ip2 = "88.87.192.0";
        // // var ip2 = "207.97.227.239";
        // var geo = geoip.lookup(ip);
        // let zona = comunidades.get(geo.region);
        // if(!zona){
        //     zona = paises.find((pais) => pais.iso2 === geo.country);
        //     zona = zona.nombre;
        // }
        // var localidad = geo.city + ', ' + zona;
        // // console.log(localidad, geo);
        
        // let usuarioBD = await User.findByIdAndUpdate(
        //     uid,
        //     { ultimoAcceso: ultimoAcceso, sitioConexion: localidad},
        //     { new: true }
        // );

        let usuarioBD = await User.findByIdAndUpdate(
            uid,
            { ultimoAcceso: ultimoAcceso},
            { new: true }
        );

        // console.log(usuarioBD);


        res.status(200).json({
            ok: true,
            msg: 'Actualización con éxito',
            data: usuarioBD
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error actualizando usuario'+ error,
            message: error
        });
    }
}

const eliminarUsuario = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || ((!req.headers.hasOwnProperty('uid')) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol === 'cliente')){
        return res.status(401).send("Sin autorización");
    }

    try {
        const uid = req.params.usuarioId;

        let usuarioBD = await User.findOneAndDelete({_id: uid}).select("-password");

        if (usuarioBD === null) {
            usuarioBD = await Client.findOneAndDelete({_id: uid}).select("-password");
        }

        if(usuarioBD.rol === 'cliente') {
            await User.findOneAndUpdate({
                _id: usuarioBD.respComercial,
                numClientes: { $gt: 0 }
            }, {
                $inc: {
                    'numClientes': -1
                }
            });
        }

        res.status(200).json({
            ok: true,
            msg: 'eliminarUsuario',
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error eliminando usuario',
            message: error
        });
    }
}


const loginUsuario =  async (req, res) => {
    const { username, password } = req.body;

    // console.log(username, password);

    try {
        let usuarioBD = await User.findOne({ username });

        if (!usuarioBD || !usuarioBD.activo) {
            return res.status(400).json({
                ok: false,
                error: 1,
                msg: 'Usuario o contraseña incorrectos',
            });
        }

        const validPassword = bcrypt.compareSync(password, usuarioBD.password);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Usuario o contraseña incorrectos',
            });
        }
        else {
            const token = jwt.sign({ _id: usuarioBD._id, nombre: usuarioBD.nombre}, process.env.TOKENSECRET, { expiresIn: 24 * 60 * 60 });
            return res.status(200).json({ token, rol: usuarioBD.rol, uid: usuarioBD._id});
        }
    } catch (error) {
        return res.status(400).json({
            ok: false,
            msg: 'Error en login',
        });
    }
};

const statusUsuario = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol !== 'admin'){
        return res.status(401).send("Sin autorización");
    }
    let mensaje, statusMsg;

    try {
        const estado = req.body.activo;
        const usuarioId = req.params.usuarioId;
        let usuario;

        if(estado){
            mensaje = 'activar usuario';
            statusMsg = ' Error activando usuario';
        }
        else {
            mensaje = 'desactivar usuario';
            statusMsg = ' Error desactivando usuario';
        }

        usuario = await User.findOneAndUpdate(
            { _id: usuarioId },
            { activo: estado },
            { new: true }
        );        

        res.status(200).json({
            ok: true,
            msg: mensaje,
            data: usuario
        });
    } catch (error) {
        res.status(500).json({
            status: statusMsg + error,
            message: error
        });
    }    
}

const getIpUsuario = async (req, res) => {
    if(!(verifyToken(req.headers.token)) || (!req.headers.hasOwnProperty('rol')) || req.headers.rol === 'cliente'){
        return res.status(401).send("Sin autorización");
    }

    try {
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        // const clientIP = req.headers['x-forwarded-for']?.split(',').shift() || req.socket?.remoteAddress;

        // res.json({ ip });

        var ip2 = "207.97.227.239";
        var geo = geoip.lookup(ip2);
        // console.log(geo);
        
        res.status(200).json({
            ok: true,
            ip: ip,
        });
    } catch (error) {
        res.status(500).json({
            status: 'Error obteniendo ip del usuario' + error,
            message: error
        });
    }    
}


module.exports = { registrarUsuario, loginUsuario, getUsuarios, getUsuario, actualizarUsuario, eliminarUsuario, actualizarUltimoAcceso, 
    statusUsuario, getIpUsuario, getUsuariosEspecialidad }
