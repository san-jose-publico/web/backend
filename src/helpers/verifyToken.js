const jwt = require('jsonwebtoken');

const verifyToken = function(token){
    try {
        const payload = jwt.verify(token, process.env.TOKENSECRET);
        return true;

      } catch (error) {
        return false;
    }
}

module.exports = { verifyToken }