const { Schema, model } = require('mongoose');

const incidenciaSchema = new Schema({
    codigo: {
        type: String,
        require: true,
    },
    autor: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        require: true
    },
    fechaCreacion: {
        type: Date,
    },
    disciplina: {
        type: Array,
        require: true
    },
    especialidad: {
        type: String,
        require: true
    },
    estado: {
        type: String,
        require: true
    },
    descripcion: {
        type: String,
        require: true
    },
    complemento1: {
        type: String
    },
    complemento2: {
        type: String
    },
    documentos: {
        fotos: {
            type: Array
        },
        videos: {
            type: Array
        },
        audios: {
            type: Array
        },
    },
    blockChainId: {
        type: String
    },
    activo: {
        type: Boolean, 
        require: true
    },
    proyecto: {
        type: Schema.Types.ObjectId,
        ref: 'Proyecto',
        require: true
    },
    fechaEstadoCerrada: {
        type: Date,
    },
    activo: {
        type: Boolean
    },
}, {collection: 'incidencias'});

incidenciaSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject();
    object.uid = _id;

    return object
})


module.exports = model('Incidencia', incidenciaSchema);