const { Schema, model } = require('mongoose');

const inspeccionSchema = new Schema({
    codigo: {
        type: String,
        require: true,
    },
    autor: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        require: true
    },
    fechaCreacion: {
        type: Date,
        require: true
    },
    descripcion: {
        type: String,
        require: true
    },
    complemento1: {
        type: String
    },
    complemento2: {
        type: String
    },
    estado: {
        type: String,
        require: true
    },
    documentos: {
        fotos: {
            type: Array
        },
        videos: {
            type: Array
        },
        audios: {
            type: Array
        },
    },
    activo: {
        type: Boolean, 
        require: true
    },
    incidencia: {
        type: Schema.Types.ObjectId,
        ref: 'Incidencia',
        require: true
    },
    proyecto: {
        type: Schema.Types.ObjectId,
        ref: 'Proyecto',
    },
    activo: {
        type: Boolean
    },
}, {collection: 'inspecciones'});

inspeccionSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject();
    object.uid = _id;

    return object
})


module.exports = model('Inspeccion', inspeccionSchema);