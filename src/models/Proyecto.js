const { Schema, model } = require('mongoose');

const projectSchema = new Schema({
    codigo: {
        type: String,
        require: true,
    },
    nombre: {
        type: String,
        require: true
    },
    autor: {
        type: String,
        require: true
    },
    fechaCreacion: {
        type: Date,
    },
    documentos: {
        type: Array
    },
    activo: {
        type: Boolean, 
        require: true
    },
    numIns: {
        type: Number,
        default: 0
    },
    numInc: {
        type: Number,
        default: 0
    }

}, {collection: 'proyectos'});

projectSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject();
    object.uid = _id;

    return object
})


module.exports = model('Proyecto', projectSchema);