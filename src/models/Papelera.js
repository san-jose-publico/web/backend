const { Schema, model } = require('mongoose');

const papeleraSchema = new Schema({
    objetoId: {
        type: String,
        require: true,
    },
    nombre: {
        type: String,
        require: true,
    },
    fechaEliminacion: {
        type: Date,
        require: true
    },
    tipo: {
        type: String,
        require: true
    },
}, {collection: 'papelera'});

papeleraSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject();
    object.uid = _id;

    return object
})


module.exports = model('Papelera', papeleraSchema);