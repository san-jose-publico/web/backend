const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    username: {
        type: String,
        require: true,
        unique: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true
    },
    nombre: {
        type: String,
        require: true
    },
    activo: {
        type: Boolean, 
        require: true
    },
    apellidos: {
        type: String,
        require: true
    },
    rol: {
        type: String,
        require: true
    },
    especialidad: {
        type: String,
        require: true
    },
    fechaAlta: {
        type: Date,
    },
    ultimoAcceso: {
        type: Date,
    },


}, {collection: 'users'});

userSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject();
    object.uid = _id;

    return object
})


module.exports = model('Usuario', userSchema);