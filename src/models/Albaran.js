const { Schema, model } = require('mongoose');

const albaranSchema = new Schema({
    numero: {
        type: String,
        require: true,
        unique: true
    },
    foto: {
        type: String,
        require: true
    },
    m3: {
        type: String,
        require: true
    },
    proveedor: {
        type: String,
        require: true
    },
    tipoHormigon: {
        type: String,
    },
    otro: {
        type: String,
    },
    activo: {
        type: Boolean
    },
}, {collection: 'albaranes'});

albaranSchema.method('toJSON', function(){
    const {__v, _id, ...object} = this.toObject();
    object.uid = _id;

    return object
})


module.exports = model('Albaran', albaranSchema);