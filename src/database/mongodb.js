const mongoose = require('mongoose');


const connection = mongoose.connect(process.env.MONGO_URI + process.env.DATABASE_NAME, {

// const connection = mongoose.connect(process.env.MONGO_LOCAL_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(db => console.log("Connected to MongoDB"))
.catch(err => console.log("Error connecting db: " + err));

module.exports = connection;