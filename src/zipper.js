const fs = require('fs');
const path = require('path');
const archiver = require('archiver');

const sourceFolder = '.'; // Puedes cambiar esto según tu estructura de carpetas
const zipFileName = process.argv[2] || 'archivos.zip';
const prodVersionsFolder = 'prod_versions';

async function createZip() {
  const output = fs.createWriteStream(zipFileName);
  const archive = archiver('zip', { zlib: { level: 9 } });

  // Pipe de la salida del archivo principal
  archive.pipe(output);

  // Función recursiva para agregar archivos y carpetas al zip
  async function addToZip(folder) {
    const files = fs.readdirSync(folder);

    for (const file of files) {
      const filePath = path.join(folder, file);

      if (file !== 'node_modules' && file !== 'prod_versions') {
        if (fs.statSync(filePath).isDirectory()) {
          await addToZip(filePath);
        } else {
          // Agregar el archivo al zip principal con la ruta relativa
          archive.file(filePath, { name: path.relative(sourceFolder, filePath) });

          // Si el archivo es un .zip, muevelo a la carpeta prod_versions
          if (path.extname(filePath) === '.zip') {
            const destinationPath = path.join(prodVersionsFolder, path.basename(filePath));
            fs.renameSync(filePath, destinationPath);
            console.log(`Movido ${filePath} a ${destinationPath}`);
          }
        }
      }
    }
  }

  // Iniciar el proceso de agregar archivos y carpetas al zip
  await addToZip(sourceFolder);

  // Finalizar el archivo zip principal
  archive.finalize();
  console.log('Proceso finalizado');
}

// Llamar a la función para crear el archivo zip
createZip();
