const express = require('express');
const app = express();
const cors = require('cors');

require('dotenv').config({path: __dirname + '/env/.env'});
require('./database/mongodb')

app.set('port', process.env.PORT || 8080);

app.use(cors());
app.use(express.json());

app.use('/api/usuarios', require('./routes/user'));
app.use('/api/inspecciones', require('./routes/inspeccion'));
app.use('/api/incidencias', require('./routes/incidencia'));
app.use('/api/proyectos', require('./routes/proyecto'));
app.use('/api/media', require('./routes/media'))
app.use('/api/especialidades', require('./routes/especialidad'));
app.use('/api/albaranes', require('./routes/albaran'));
app.use('/api/papelera', require('./routes/papelera'));
app.use('/api/email', require('./routes/email'));

app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});